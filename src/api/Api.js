import { Buffer } from "buffer";
import { io } from "socket.io-client";
import Line from "../curves/line";

export default class Api {
  constructor() {
    this.subFunctions = [];
  }

  connect(url) {
    this.socket = io(url, { transports: ["websocket"] });
  }

  subscribe(func) {
    this.subFunctions = [...this.subFunctions, func];
  }

  unsubscribe(func) {
    const index = this.subFunctions.findIndex((f) => f.name == func.name);
    this.subFunctions.splice(index);
  }

  listen(
    model,
    updateConnection,
    handleRoomCreated,
    updateCanvas,
    updateMessages,
    addAttribute,
    addAttributes,
    updateAttribute,
    removeAttribute
  ) {
    this.socket.on("connect", () => {
      updateConnection();
    });

    this.socket.on("disconnect", (reason) => {
      alert(`Disconnected from server, reason: ${reason}`);
    });

    this.socket.on("error", (reason) => {
      alert(`Disconnected from server, reason: ${reason}`);
    });

    this.socket.on("room-created", (data) => {
      handleRoomCreated(data.room_id);
    });

    this.socket.on("room-joined", (room_id) => {
      handleRoomCreated(room_id);
    });

    this.socket.on("update-model", (_model) => {
      const edges = [];
      const vertices = [..._model.vertices];
      const meshes = [..._model.meshes];

      _model.edges.forEach((edge) => {
        let new_line = new Line(
          edge.points[0][0],
          edge.points[0][1],
          edge.points[1][0],
          edge.points[1][1],
          edge.attributes
        );
        new_line.selected = edge.selected;
        edges.push(new_line);
      });

      model.curves = [];
      model.curves = edges;
      model.vertices = vertices;
      model.meshes = meshes;
      updateCanvas();

      this.getTriangs();
      // this.getAttributeSymbols();
    });

    this.socket.on("tesselation", (data) => {
      model.patches = data;
      updateCanvas();
    });

    this.socket.on("message", (message) => {
      updateMessages(message);
    });

    this.socket.on("receive-prototypes", (prototypes) => {
      this.subFunctions[0](prototypes);
    });

    this.socket.on("add-attribute", (attribute) => {
      console.log("add-attribute");
      //addAtribute(attribute);
    });

    this.socket.on("receive-attribute", (attribute) => {
      addAttribute(attribute);
    });

    this.socket.on("receive-attributes", (attributes) => {
      addAttributes(attributes);
    });

    this.socket.on("update-attribute", (attribute) => {
      updateAttribute(attribute);
    });

    this.socket.on("remove-attribute", (attributeName) => {
      removeAttribute(attributeName);
    });

    this.socket.on("download-file", (jsonModel) => {
      const downloadElement = document.createElement("a");
      const file = new Blob([jsonModel], { type: "application/json" });
      downloadElement.href = URL.createObjectURL(file);
      downloadElement.download = this.socket.id;
      downloadElement.click();
    });
  }

  saveFile() {
    this.socket.emit("save-file");
  }

  loadFile(file) {
    this.socket.emit("load-file", file);
  }

  insertCurve(curve) {
    this.socket.emit("insert-curve", curve);
  }

  createRoom() {
    this.socket.emit("create-room");
  }

  joinRoom(token) {
    this.socket.emit("join-room", token);
  }

  selectFence(xmin, xmax, ymin, ymax, isShiftPressed) {
    this.socket.emit("select-fence", xmin, xmax, ymin, ymax, isShiftPressed);
  }

  selectPick(x, y, tol, isShiftPressed) {
    this.socket.emit("select-pick", x, y, tol, isShiftPressed);
  }

  delSelectedEntities() {
    this.socket.emit("delete-selected-entities");
  }

  intersect() {
    this.socket.emit("intersect");
  }

  getTriangs() {
    this.socket.emit("tesselation");
  }

  sendMessage(message) {
    this.socket.emit("message", message);
  }

  getPrototypes(handle_prototypes) {
    this.socket.emit("get-prototypes");
    this.subscribe(handle_prototypes);
  }

  applyAttribute(attribute) {
    this.socket.emit("apply-attribute", attribute);
  }

  createAttribute(attribute) {
    this.socket.emit("create-attribute", attribute);
  }

  updateAttribute(attribute) {
    this.socket.emit("update-attribute", attribute);
  }

  removeAttribute(attributeName) {
    this.socket.emit("remove-attribute", attributeName);
  }

  getAttributeSymbols() {
    this.socket.emit("get-attribute-symbols");
  }

  generateMesh(meshInfo) {
    this.socket.emit("generate-mesh", meshInfo);
  }
}
